import { defineConfig } from 'vite';
import { crx, defineManifest } from '@crxjs/vite-plugin';
import react from '@vitejs/plugin-react';
import * as path from 'path';

const manifest = defineManifest({
  manifest_version: 3,
  name: 'CHESS TRAPPER',
  version: '0.0.0',
  action: { default_popup: 'index.html' },
  content_scripts: [{
    matches: ['http://tsukifunemira.sakura.ne.jp/udonamism/'],
    run_at: 'document_start',
    js: ['./src/content/index.tsx']
  }],
  background: {
    service_worker: './src/background/index.tsx',
    type: 'module'
  }
});

export default defineConfig({
  plugins: [react(), crx({ manifest })],
  base: './',
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src')
    }
  }
})

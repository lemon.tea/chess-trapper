import { v4 as uuid } from 'uuid';

// 罠
export type Trap = {
  id: string;
  row: number;
  col: number;
  type: 'SLIP'|'CWEB'|'VOLT';
};

// 罠を作成する
export function createTrap(row:number, col:number, type:'SLIP'|'CWEB'|'VOLT') {
  return {
    id: uuid(),
    row: row,
    col: col,
    type: type,
  }
}
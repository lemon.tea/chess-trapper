import { Trap } from './trap';
import { Camp } from './camp';

// 罠管理テーブル
export type TrapManager = {
  traps: Trap[];
  camps: Camp[];
}

// 配列から罠を取り除く
function tryRemove(traps:Trap[], trap:Trap) {
  const index = traps.findIndex((v,_i,_a) => v.id === trap.id);
  if(index !== -1) { traps.splice(index, 1); }
};

// 配列中の罠を置き換える
function tryReplace(traps:Trap[], trap:Trap) {
  const index = traps.findIndex((v,_i,_a) => v.row === trap.row && v.col === trap.col);
  if(index !== -1) { traps.splice(index, 1); }
  traps.push(trap);
};

// 陣営の名前を取得する
export function getCampList(manager:TrapManager) {
  return manager.camps.map((v,_i,_a) => v.name);
}

// 陣営の名前から陣営を取得する
export function getCampByName(manager:TrapManager, name:string) {
  const camp = manager.camps.find((v,_i,_a) => v.name === name);
  if(!camp) { throw new Error('The camp of the name not found.'); }
  return camp;
}

// 指定した陣営に罠を設置する
export function locate(manager:TrapManager, camp:Camp, trap:Trap) {
  tryReplace(manager.traps, trap);
  tryReplace(camp.traps, trap);
}

// 指定した陣営から罠を取り除く
export function remove(manager:TrapManager, trap:Trap) {
  tryRemove(manager.traps, trap);
  for(let camp of manager.camps) {
    tryRemove(camp.traps, trap);
  }
}

// 指定した罠を指定した陣営に公開する
export function visualize(manager:TrapManager, trap:Trap, camp:Camp) {

  // 入力した罠が実際に設置されていた場合は置き換える
  if(manager.traps.some((v,_i,_a) => v.id === trap.id)) {
    tryReplace(camp.traps, trap);
  }

  // そうでない場合は削除する
  else {
    for(let camp of manager.camps) {
      tryRemove(camp.traps, trap);
    }
  }

}
import { v4 as uuid } from 'uuid';

import { Trap } from './trap';

// 陣営
export type Camp = {
  id: string;
  name: string;
  traps: Trap[];
};

// 陣営を作成する
export function createCamp(name:string) {
  return {
    id: uuid(),
    name: name,
    traps: []
  }
}
import { Trap } from '../trap/trap';

import styles from './game-trap.module.scss';

import slip from '@/assets/slip.png';
import cweb from '@/assets/cweb.png';
import volt from '@/assets/volt.png';

type GameTrapProps = {
  hidden: boolean,
  trap: Trap
}

export default function GameTrap(props:GameTrapProps) {

  const x = 100 * props.trap.col + 105;
  const y = 100 * props.trap.row + 105;
  const style = {
    transform: `translate3d(${x}px, ${y}px, 0px)`,
    opacity: props.hidden ? '0.25' : '1.0'
  };

  const image = (trap:'SLIP'|'CWEB'|'VOLT') => {
    switch (trap) {
      case 'SLIP': return slip;
      case 'CWEB': return cweb;
      case 'VOLT': return volt;
      default: throw new Error('Invalid trap type.');
    }
  }

  return (
    <div className={styles['game-trap']} style={style}>
      <img src={image(props.trap.type)} alt="Image"/>
    </div>
  );
}
import ReactDOM from 'react-dom/client';

import GameTrap from './game-trap';
import { Trap } from '../trap/trap';

// メッセージの型
type Message = {
  visibleTraps: Trap[];
  hiddenTraps: Trap[];
}

// イベントリスナーを追加
chrome.runtime.onConnect.addListener(port => {

  // メッセージを受け取った時の処理
  port.onMessage.addListener((message, port) => {

    // メッセージを変換する
    const msg = message as Message;
    
    // 要素を生成する
    const visibleTraps = msg.visibleTraps.map((v,_i,_a) => <GameTrap hidden={false} trap={v}/>);
    const hiddenTraps = msg.hiddenTraps.map((v,_i,_a) => <GameTrap hidden={true} trap={v}/>);
    const traps = visibleTraps.concat(hiddenTraps);

    // マーカーを配置する要素を取得or生成する
    let markers = document.getElementById('trap-markers');
    if(markers === null) {
      const table = document.getElementById('app-game-table');
      if(table !== null) {
        markers = document.createElement('div');
        markers.id = 'trap-markers';
        markers.style.position = 'absolute';
        table.appendChild(markers);
      } else {
        throw new Error('This page is not Udonarium.');
      }
    }

    // ルートを作成してレンダリング
    const root = ReactDOM.createRoot(markers);
    root.render(traps);

  });

});

export {};
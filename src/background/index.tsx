import { TrapManager } from '../trap/manager';

// ストアする罠管理テーブル;
let manager:TrapManager|undefined = undefined;

// メッセージの型
type Message = {
  action: string;
  content: TrapManager|undefined;
}

// イベントリスナーを追加
chrome.runtime.onConnect.addListener(port => {

  // メッセージを受け取った時の処理
  port.onMessage.addListener((message, port) => {

    // 受け取った action によって処理を変更
    const msg = message as Message;
    if(msg.action === 'load') { port.postMessage(manager); }
    if(msg.action === 'store') { manager = msg.content; }

  });

});

export {};
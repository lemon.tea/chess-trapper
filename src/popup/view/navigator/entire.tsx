import { useContext } from 'react';

import { campContext } from '@/popup/contexts/camp';

import neumorph from '@/popup/styles/neumorph.module.scss';
import styles from './entire.module.scss';

type NavigatorEntireProps = {
  key: string;
}

export default function NavigatorEntire(props:NavigatorEntireProps) {

  // 陣営情報を取得
  const { campName, setCampName } = useContext(campContext);
  if(campName === undefined || setCampName === undefined) {
    return <></>;
  }

  // 陣営の切り替え
  const changeCamp = () => {
    setCampName('');
  };

  // レンダリング
  return (
    <div className={styles['camp-item']}>
      <div className={`${styles['camp-name']} ${neumorph['neumorph']} ${neumorph[campName === '' ? 'concave' : 'convex']}`} onClick={changeCamp}>全体</div>
      <div className={styles['blank']}/>
    </div>
  );
}
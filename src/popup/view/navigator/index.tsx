import { useContext } from 'react';

import { trapContext } from '@/popup/contexts/trap';
import NavigatorItem from '@/popup/view/navigator/item';
import NavigatorEntire from '@/popup/view/navigator/entire';

import styles from './index.module.scss';

export default function Navigator() {

  // 罠管理テーブルを取得
  const { manager, setManager } = useContext(trapContext);
  if(manager === undefined || setManager === undefined) {
    return <></>;
  }

  // 要素を生成する
  const items = manager.camps.map((v,_i,_a) => <NavigatorItem key={v.id} camp={v}/>);
  items.splice(0, 0, <NavigatorEntire key={''}/>);

  // レンダリング
  return (
    <div>{items}</div>
  );

}
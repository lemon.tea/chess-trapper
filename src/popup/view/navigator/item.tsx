import { useContext } from 'react';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

import { Camp } from '@/trap/camp';

import { IconButton } from '@/popup/components/icon-button';

import { trapContext } from '@/popup/contexts/trap';
import { campContext } from '@/popup/contexts/camp';

import neumorph from '@/popup/styles/neumorph.module.scss';
import styles from './item.module.scss';

type NavigatorItemProps = {
  key: string;
  camp: Camp;
};

export default function NavigatorItem(props:NavigatorItemProps) {

  // 罠管理テーブルを取得
  const { manager, setManager } = useContext(trapContext);
  if(manager === undefined || setManager === undefined) {
    return <></>;
  }

  // 陣営情報を取得
  const { campName, setCampName } = useContext(campContext);
  if(campName === undefined || setCampName === undefined) {
    return <></>;
  }

  // 陣営の切り替え
  const changeCamp = () => {
    setCampName(props.camp.name);
  };

  // 陣営の削除
  const deleteCamp = () => {
    const index = manager.camps.findIndex((v,_i,_a) => v.id === props.camp.id);
    if(index !== -1) { manager.camps.splice(index, 1); }
    setCampName('');
    setManager({...manager});
  };

  // レンダリング
  return (
    <div className={styles['camp-item']}>
      <div className={`${styles['camp-name']} ${neumorph['neumorph']} ${neumorph[campName === props.camp.name ? 'concave' : 'convex']}`} onClick={changeCamp}>{props.camp.name}</div>
      <IconButton icon={faTrash} onClick={deleteCamp}/>
    </div>
  );
}
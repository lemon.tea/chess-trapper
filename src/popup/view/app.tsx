import { useEffect, useState } from 'react';

import { getCampByName, getCampList, TrapManager } from '@/trap/manager';
import { createCamp } from '@/trap/camp';

import { TextButton } from '../components/text-button';

import { TrapContext } from '@/popup/contexts/trap';
import { CampContext } from '@/popup/contexts/camp';
import Navigator from '@/popup/view/navigator';
import Content from '@/popup/view/content';

import styles from './app.module.scss';
import { TextBox } from '../components/text-box';

// バックグラウンドスクリプトとの接続
export let backgroundPort = chrome.runtime.connect();
 
// コンテンツスクリプトとの接続
export let contentPort:chrome.runtime.Port;
let query = { active:true, currentWindow:true };
chrome.tabs.query(query, tabs => {
  if(tabs[0].id) {
    contentPort = chrome.tabs.connect(tabs[0].id);
  }
});

export default function App() {

  // 状態の生成
  const [campName, setCampName] = useState('');
  const [newCamp, setNewCamp] = useState('');
  const [manager, setManager] = useState<TrapManager>({traps:[],camps:[]});

  // 新しい陣営を追加する
  const addNewCamp = () => {
    if(newCamp === '') {
      alert('陣営の名前を入力してください。');
    }
    else if(getCampList(manager).includes(newCamp)) {
      alert('その名前は既に入力されています。他の名前を使用してください。');
    }
    else {
      manager.camps.push(createCamp(newCamp));
      setManager({...manager});
    }
    setNewCamp('');
  };

  // 初回次に実行する副作用
  useEffect(() => {

    // 罠管理テーブルを読み込む
    if(backgroundPort) {
      const backgroundMsg = { action:'load' };
      const listener = (response:any, port:chrome.runtime.Port) => {
        if(response) { setManager(response as TrapManager); }
        backgroundPort.onMessage.removeListener(listener);
      };
      backgroundPort.onMessage.addListener(listener);
      backgroundPort.postMessage(backgroundMsg);
    }

  }, []);

  // 毎回実行する副作用
  useEffect(() => {

    // 罠管理テーブルを保存する
    if(backgroundPort) {
      const backgroundMsg = { action:'store', content:manager };
      backgroundPort.postMessage(backgroundMsg);
    }

    // Udonarium の DOM を更新する
    if(contentPort) {
      const visibleTraps = campName === '' ? manager.traps : getCampByName(manager, campName).traps;
      const hiddenTraps = manager.traps.filter((v,_i,_a) => !visibleTraps.some((u,_i,_a) => v.id === u.id));
      const contentMsg = { visibleTraps:visibleTraps, hiddenTraps:hiddenTraps };
      contentPort.postMessage(contentMsg);
    }

  }, [manager, campName]);

  // レンダリング
  return (
    <TrapContext context={{ manager, setManager }}>
      <CampContext context={{ campName, setCampName }}>
        <div className={styles['page-top']}>
          <div className={styles['navigate-page']}>
            <Navigator/>
          </div>
          <div className={styles['content-page']}>
            <Content/>
            <div className={styles['add-camp']}>
              <TextBox value={newCamp} label='' onChange={setNewCamp}/>
              <TextButton text="陣営の追加" onClick={addNewCamp}/>
            </div>
          </div>
        </div>
      </CampContext>
    </TrapContext>
  );
}
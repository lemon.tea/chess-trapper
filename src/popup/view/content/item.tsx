import { useContext } from 'react';

import { faEye, faTrash } from '@fortawesome/free-solid-svg-icons';

import { Trap } from '@/trap/trap';
import { getCampByName, remove, visualize } from '@/trap/manager';

import { IconButton } from '@/popup/components/icon-button';

import { trapContext } from '@/popup/contexts/trap';
import { campContext } from '@/popup/contexts/camp';

import neumorph from '@/popup/styles/neumorph.module.scss';
import styles from './item.module.scss';

type ContentItemProps = {
  key: string,
  trap: Trap,
  traps: Trap[],
};

export default function ContentItem(props:ContentItemProps) {

  // 罠管理テーブル(null 非許容)を取得
  const { manager, setManager } = useContext(trapContext);
  if(manager === undefined || setManager === undefined) {
    return <></>;
  }

  // 陣営情報を取得
  const { campName, setCampName } = useContext(campContext);
  if(campName === undefined || setCampName === undefined) {
    return <></>;
  }

  // 列のインデックスを文字列で取得する
  const index = (row:number,col:number):string => {
    let colIndex = '';
    let colTable = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    while(true) {
      colIndex += colTable[col % colTable.length];
      col -= col % colTable.length;
      col /= colTable.length;
      if(col === 0) break;
    }
    return `${colIndex}${row+1}`;
  }

  // 罠の名前を取得する
  const name = (code:'SLIP'|'CWEB'|'VOLT') => {
    switch(code) {
      case 'SLIP': return 'スリップトラップ';
      case 'CWEB': return 'キャプチャーウェブ';
      case 'VOLT': return 'サンダーボルト';
      default: throw new Error('Invalid trap code.');
    }
  }

  // 罠を削除する時の処理
  const removeTrap = () => {
    remove(manager, props.trap);
    setManager({...manager});
  };

  // 罠を公開する時の処理
  const resolveTrap = () => {
    const camp = getCampByName(manager, campName);
    visualize(manager, props.trap, camp);
    setManager({...manager});
  };

  // レンダリング
  return (
    <div className={styles['trap-item']}>
      <div className={`${styles['trap-info']} ${neumorph['neumorph']} ${neumorph[props.traps.includes(props.trap) ? 'convex' : 'concave']}`}>
        {index(props.trap.row,props.trap.col)} - {name(props.trap.type)}
      </div>
      {
        campName === '' || props.traps.includes(props.trap) ?
        <div className={styles['blank']}/> :
        <IconButton icon={faEye} onClick={resolveTrap}/>
      }
      <IconButton icon={faTrash} onClick={removeTrap}/>
    </div>
  );

}
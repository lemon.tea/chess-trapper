import { useContext, useState } from 'react';

import { getCampByName, locate } from '@/trap/manager';
import { createTrap } from '@/trap/trap';

import { TextButton } from '@/popup/components/text-button';
import { TextBox } from '@/popup/components/text-box';

import { trapContext } from '@/popup/contexts/trap';
import { campContext } from '@/popup/contexts/camp';
import ContentItem from '@/popup/view/content/item';

import styles from './index.module.scss';
import { SelectBox } from '@/popup/components/select-box';

export default function Content() {

  // 罠管理テーブル(null 非許容)を取得
  const { manager, setManager } = useContext(trapContext);
  if(manager === undefined || setManager === undefined) {
    return <></>;
  }

  // 陣営情報を取得
  const { campName, setCampName } = useContext(campContext);
  if(campName === undefined || setCampName === undefined) {
    return <></>;
  }

  // 罠の情報
  const [cell, setCell] = useState('');
  const [trap, setTrap] = useState<'SLIP'|'CWEB'|'VOLT'>('SLIP');

  // 罠を設定する時のコールバック
  const onChangeTrap = (value:string) => {
    switch (value) {
      case 'SLIP':
      case 'CWEB':
      case 'VOLT':
        setTrap(value);
        break;
      default:
        throw new Error('Invalid trap type.');
    }
  };

  // 列挙する罠一覧を取得
  let traps = campName === '' ? manager.traps : getCampByName(manager, campName).traps;

  // 罠一覧を生成
  const visibleItems = traps.map((v,_i,_a) => <ContentItem key={v.id} traps={traps} trap={v}/>);
  const hiddenItems = manager.traps
    .filter((v,_i,_a) => !traps.some((u,_i,_a) => v.id === u.id))
    .map((v,_i,_a) => <ContentItem key={v.id} traps={traps} trap={v}/>);

  // 罠を追加する時の処理
  const locateTrap = () => {

    // 設置位置を整形
    let tmp = cell;
    tmp = tmp.replace(/[Ａ-Ｚａ-ｚ０-９]/g, s => String.fromCharCode(s.charCodeAt(0) - 0xFEE0));
    tmp = tmp.toUpperCase();

    // 形式に従うなら設置
    let match = tmp.match(/^([A-Z]+)([0-9]+)$/);
    if(match && match.length > 0) {

      // 列情報を数値に変換する
      const toIndex = (s:string) => {
        const target = s.charCodeAt(0);
        const base = 'A'.charCodeAt(0);
        return target - base;
      }

      // 文字列を数値に変換
      const [_, colStr, rowStr] = match;
      const row = parseInt(rowStr) - 1;
      const col = [...colStr].reduce((p,c,i,a) => 26 * p + toIndex(c), 0);

      // 罠管理テーブルを更新
      locate(manager, getCampByName(manager, campName), createTrap(row, col, trap));
      setManager({...manager});

    }

    // そうでない場合はアラート
    else {
      alert('罠の配置情報の形式が違います');
    }
  }

  // レンダリング
  return (
    <div className={styles['content']}>
      <div className={styles['items']}>{visibleItems}{hiddenItems}</div>
      {campName === '' ? <></> : (
        <div className={styles['add-trap']}>
          <TextBox label='' value={cell} onChange={setCell}/>
          <SelectBox value={trap} items={['SLIP','CWEB','VOLT']} names={['スリップトラップ','キャプチャーウェブ','サンダーボルト']} onChange={onChangeTrap}/>
          <TextButton text="罠の追加" onClick={locateTrap}/>
        </div>
      )}
    </div>
  );

}
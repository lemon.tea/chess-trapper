import React from 'react';
import ReactDOM from 'react-dom/client';
import App from '@/popup/view/app';

import './main.scss';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
)

import { createContext } from 'react';

import { TrapManager } from '@/trap/manager';

// コンテキスト
type Context = {
  manager?: TrapManager;
  setManager?: (manager:TrapManager) => void;
};

// コンテキストを生成
export const trapContext = createContext<Context>({});

// コンテキストを与える要素のプロパティ
type TrapContextProps = {
  children: string|JSX.Element|JSX.Element[];
  context: Context;
};

// コンテキストを与える要素
export function TrapContext(props:TrapContextProps) {
  return (
    <trapContext.Provider value={props.context||{}}>
      {props.children}
    </trapContext.Provider>
  );
}
import { createContext } from 'react';

// コンテキスト
type Context = {
  campName?: string;
  setCampName?: (camp:string) => void;
}

// コンテキストを生成
export const campContext = createContext<Context>({});

// コンテキストを与える要素のプロパティ
type CampContextProps = {
  children: string|JSX.Element|JSX.Element[];
  context: Context;
};

// コンテキストを与える要素
export function CampContext(props:CampContextProps) {
  return (
    <campContext.Provider value={props.context||{}}>
      {props.children}
    </campContext.Provider>
  );
}
import { ChangeEvent } from 'react';

import neumorph from '@/popup/styles/neumorph.module.scss';
import styles from './number-box.module.scss';

type NumberBoxProps = {
  value: number;
  label: string;
  onChange: (value:number) => void;
};

export function NumberBox(props:NumberBoxProps) {

  const onChange = (event:ChangeEvent<HTMLInputElement>) => {
    const value = parseInt(event.target.value);
    props.onChange(isNaN(value) ? 0 : value);
  }

  return (
    <input  className={`${neumorph['neumorph']} ${neumorph['convex']}`} type="number" value={props.value} onChange={onChange}/>
  );

}
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import neumorph from '@/popup/styles/neumorph.module.scss';
import styles from './icon-button.module.scss';

type ButtonProps = {
  icon: IconDefinition;
  onClick: () => void;
};

export function IconButton(props:ButtonProps) {

  return (
    <button className={`${neumorph['neumorph']} ${neumorph['convex']} ${neumorph['pressable']} ${styles['button']}`} onClick={props.onClick}>
      <div className={styles['icon-area']}>
        <FontAwesomeIcon icon={props.icon}/>
      </div>
    </button>
  );

}
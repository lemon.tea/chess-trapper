import neumorph from '@/popup/styles/neumorph.module.scss';
import styles from './text-button.module.scss';

type ButtonProps = {
  text: string;
  onClick: () => void;
};

export function TextButton(props:ButtonProps) {

  return (
    <button className={`${neumorph['neumorph']} ${neumorph['convex']} ${neumorph['pressable']}`} onClick={props.onClick}>
      {props.text}
    </button>
  );

}
import neumorph from '@/popup/styles/neumorph.module.scss';
import styles from './select-box.module.scss';

type SelectBoxProps = {
  value: string;
  items: string[];
  names: string[];
  onChange: (value:string) => void;
};

export function SelectBox(props:SelectBoxProps) {

  return (
    <select className={`${neumorph['neumorph']} ${neumorph['convex']}`} value={props.value} onChange={event => props.onChange(event.target.value)}>
      {props.items.map((v,i,_a) => <option value={v}>{props.names[i]}</option>)}
    </select>
  );
}
import { ChangeEvent} from 'react';

import neumorph from '@/popup/styles/neumorph.module.scss';
import styles from './text-box.module.scss';

type TextBoxProps = {
  value: string;
  label: string;
  onChange: (value:string) => void;
};

export function TextBox(props:TextBoxProps) {

  const onChange = (event:ChangeEvent<HTMLInputElement>) => {
    props.onChange(event.target.value);
  }

  return (
    <input className={`${neumorph['neumorph']} ${neumorph['convex']}`} type="text" value={props.value} onChange={onChange}/>
  );

}